from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction

from .models import User, Funcionario, Administrador

class FuncionarioSignUpForm(UserCreationForm):
    administrador = forms.ModelChoiceField(
        queryset=Administrador.objects.all(),
        widget=forms.Select(attrs={'class':'form-control'}),
        required=True,
    )

    class Meta(UserCreationForm.Meta):
        model = User

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_funcionario = True
        user.save()
        funcionario = Funcionario.objects.create(user=user, adm=self.cleaned_data.get('administrador'))
        funcionario.save()   
        return user

class AdministradorSignUpForms(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_administrador = True
        user.save()
        administrador = Administrador.objects.create(user=user)
        administrador.save()
        return user