from django.db import models
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import login
from django.views.generic import CreateView

from .models import User
from .forms import FuncionarioSignUpForm, AdministradorSignUpForms

# Create your views here.

class FuncionarioSignUpView(CreateView):
    model = User
    form_class = FuncionarioSignUpForm
    template_name = 'signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'funcionario'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('home')

class AdministradorSignUpView(CreateView):
    model = User
    form_class = AdministradorSignUpForms
    template_name = 'signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'administrador'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('home')


def escolha_de_signup(request):
    return render(request, 'signup.html', {})