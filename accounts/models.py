from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):
    is_funcionario = models.BooleanField(default=False)
    is_administrador = models.BooleanField(default=False)

class Administrador(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return self.user.get_username()

class Funcionario(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    adm = models.ForeignKey(Administrador, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.get_username()