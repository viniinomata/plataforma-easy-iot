from django.contrib import admin

from .models import User, Administrador, Funcionario
# Register your models here.
admin.site.register(User)
admin.site.register(Administrador)
admin.site.register(Funcionario)
