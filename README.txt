### Configurações e Senhas ###
SUPERUSER:

user: admin
password: easyiot2021

### Instruções Básicas ###
* criar um diretório qualquer diferente de easy_iot (ex: Trabalho_Final, cd/Deskto/Trabalho_Final)
* instalar o pipenv (pip install pipenv)
* criar o ambiente virtual: pipenv install django
* iniciar: pipenv shell
* puxar o repositório: git clone https://gitlab.uspdigital.usp.br/pedrobacelarsantos/easy_iot

#------------------ GIT ------------------
## First Push changes to gitlab ##
git remote rename origin old-origin
git remote add origin https://gitlab.uspdigital.usp.br/pedrobacelarsantos/easy_iot.git
git push -u origin --all
git push -u origin --tags

## Push Changes ##
git push

## Pull changes ##
git pull 

## Criar uma Branch ##
git checkout -b <YOUR_NEW_BRANCH>

## Entrar numa Branch existente ##
git checkout <BRANCH_NAME>

## Dar Merge na Branch ao Master ##
git checkout master
git merge <BRANCH_NAME>


#------------------ DJANGO ------------------
### Criar um SUPERUSER ###
python manage.py createsuperuser


### Criar um novo APP ###
python manage.py startapp [name]
---> adicionar [name] em setting.py em APPS <---
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'name',
]

---> adicionar path em urls.py <---
from django.urls import path,include
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('name.urls')),
]

---> Criar urls.py dentro do diretório do novo APP <---

Rotina Padrão a partir daí... 
Criar HTM -> Criar View -> Adicionar url

---> Novo Modelo Criado <---
from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(name)

---> Alterações Feitas no Modelo <---
python manage.py makemigrations
python manage.py migrate
python manage.py runserver

----> Como criar eventos a partir de GET request <---
Coloca no navegador o link: "/addevent" com as informações do evento da seguinte maneira:

http://127.0.0.1:8000/addevent/?auth_token=ghi&message=teste&device_status=ok&type=checagem

Para separar as variaveis precisa utilizar o caractere '&'

---------------------- HEROKU ----------------------



---------------------- Resolução de Erros ----------------------
---> ERROR:
raise InvalidTemplateLibrary(
django.template.library.InvalidTemplateLibrary: Invalid template library specified. ImportError raised when trying to load 'rest_framework.templatetags.rest_framework': No 
module named 'pytz'

<--- Solution
pipenv install pytz


---> ERROR:
OSError: [WinError 123] The filename, directory name, or volume label syntax is incorrect: '<frozen importlib._bootstrap>' (Django) [closed]

<--- Solution
pipenv shell (nunca esqueça de sempre statar o virtual enviorment antes de começar os trabalhos)







