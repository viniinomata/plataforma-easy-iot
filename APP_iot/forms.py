from django import forms
from .models import *


class DeviceForm(forms.ModelForm):
    class Meta:
        model = Device
        fields = ('name', 'types', 'description', 'auth_token', 'image_link')

        widgets ={
            'name': forms.TextInput(attrs = {'class': 'form-control'}),
            'description': forms.TextInput(attrs = {'class': 'form-control'}),
            # 'author': forms.TextInput(attrs = {'class': 'form-control', 'value': '', 'id': 'elder', 'type': 'hidden'}),
            #'author': forms.Select(attrs = {'class': 'form-control'}),
            'auth_token': forms.TextInput(attrs = {'class': 'form-control'}),
            'image_link': forms.TextInput(attrs = {'class': 'form-control'}),
            'types': forms.SelectMultiple(attrs = {'class': 'form-control'}),
        }


class EditForm(forms.ModelForm):
    class Meta:
        model = Device
        fields = ('name', 'types', 'description', 'image_link' )

        widgets ={
            'name': forms.TextInput(attrs = {'class': 'form-control'}),
            'description': forms.TextInput(attrs = {'class': 'form-control'}),
            # 'author': forms.TextInput(attrs = {'class': 'form-control', 'value': '', 'id': 'elder', 'type': 'hidden'}),
            'image_link': forms.TextInput(attrs = {'class': 'form-control'}),
            'types': forms.SelectMultiple(attrs = {'class': 'form-control'}),

        }