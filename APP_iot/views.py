# import requests
from django.http import HttpResponse
from accounts.models import Funcionario,Administrador

from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

# from blogpacktor.models import Post
from .models import *
from .forms import *
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from accounts.decorators import funcionario_required, administrador_required
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

# Class de View para a HomePage

class HomeView(ListView):
    model = Device
    template_name = 'home.html'

    def get_queryset(self):
        if self.request.user.is_active:
            if self.request.user.is_administrador:
                administrador = Administrador.objects.get(user=self.request.user)
                object_list = Device.objects.filter(author=administrador)
                return object_list
            elif self.request.user.is_funcionario:
                object_list = Device.objects.filter(author=Administrador.objects.get(user=Funcionario.objects.get(user=self.request.user).adm))
                return object_list
        else: return []

@method_decorator([login_required, administrador_required], name='dispatch')
class AddDevicetView(CreateView):
    model = Device
    form_class = DeviceForm
    template_name = 'add_device.html'

    def form_valid(self, form):
        form.instance.author =  Administrador.objects.get(user=self.request.user)
        return  super().form_valid(form)

@method_decorator(login_required, name='dispatch')
class DetailDeviceView(DetailView):
    model = Device
    template_name = 'device_page.html'

@method_decorator([login_required, administrador_required], name='dispatch')
class EditarDeviceView(UpdateView):
    model = Device
    form_class = EditForm
    template_name = 'editar_device.html'

@method_decorator([login_required, administrador_required], name='dispatch')
class DeletarDeviceView(DeleteView):
    model = Device
    template_name = 'deletar_device.html'
    success_url = reverse_lazy('home')

@method_decorator([login_required, administrador_required], name='dispatch')
class AdicionarTypeView(CreateView):
    model = Type_Device
    template_name = 'adicionar_type.html'
    fields = '__all__'

@method_decorator(login_required, name='dispatch')
class TypeListView(ListView):
    model = Type_Device
    template_name = "type_separados.html"

@login_required
def Type_Especifico(request, type_id):
    type_especifico = get_object_or_404(Type_Device, pk = type_id)
    if request.user.is_administrador:
        administrador = Administrador.objects.get(user=request.user)
        object_list = Device.objects.filter(author=administrador, types=type_especifico)
    elif request.user.is_funcionario:
        administrador = Administrador.objects.get(user=Funcionario.objects.get(user=request.user).adm)
        object_list = Device.objects.filter(author=administrador, types=type_especifico)
    context = { 'object_list':object_list}
    return render(request, 'type_especifico.html', context)

def criar_evento(request):
    context = {}
    if request.GET.get('auth_token', False):
        device = Device.objects.get(auth_token=request.GET['auth_token'])

    if request.GET.get('device_status', False):
        device_status = request.GET['device_status']
    else:
        device_status = ""

    if request.GET.get('message', False):
        message = request.GET['message']
    else:
        message = ""

    if request.GET.get('type', False):
        tipo = request.GET['type']
    else:
        tipo = ""

    evento = Event.objects.create(device_id=device, device_status=device_status, message=message, type=tipo)
    evento.save()
    return HttpResponse("<h1>Espero ter funcionado</h1>")
    
